
Bibiana Gamba Sabogal 

Estrategia: usar separate chaining, as� al hacer put se pone el elemento en una lista encadenada en el nodo de la tabla correspondiente al hash de la llave. 

Factor de carga: promedio de objetos por lista
Complejidad del peor caso: O(N)

Al usar separate chaining cuando se repiten datos como por ejemplo mismo nombre se crea una lista encadenada en el nodo con el mismo hash ah� en este caso se logra retornar la lista con esa llave con la informaci�n que se busca.

R1
|��������������������������|
|  Entradas |   Tiempo(ms) |
|��������������������������|
|  500000   |     263.0    |
|  1000000  |     471.0    |
|  1500000  |     653.0    |                                   
|  1999998  |     2166.0   |                            
|��������������������������|

R2
|��������������������������|
|  Entradas |   Tiempo(ms) |
|��������������������������|
|  500000   |     263.0    |
|  1000000  |     471.0    |
|  1500000  |     653.0    |
|  1999998  |     2178.0   |
|��������������������������|

Si tiene sentido hacer una tabla de listas, por el hecho de que los nombres se repiten generando varias colisiones. As�, se tiene una lista por llave, haciendo que la informaci�n quede organizada por datos o elementos repetidos.

Las tablas de hash no son una buena opci�n cuando existe una alta probabilidad de que las llaves se repitan, por que se crean colisiones, se vuelve un proceso lento y existe un gasto significativo de memoria. 
