package taller.estructuras;

import java.util.Iterator;

public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private NodoHash<K, V>[] hashTable;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	public NodoHash<K, V>[] darTablaHash()
	{
		return hashTable;
	}

	public int cuenta()
	{
		return count;
	}

	public int darCapacidad()
	{
		return capacidad;
	}

	@SuppressWarnings("unchecked")
	public TablaHash()
	{
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		capacidad = 1000;
		factorCargaMax = capacidad * 1000;
		hashTable = new NodoHash[capacidad];
		count = 0;
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) 
	{
		//TODO: Inicialice la tabla con los valores dados por parametro
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		hashTable = new NodoHash[capacidad];
		count = 0;

	}

	public void put(K llave, V valor)
	{
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int pos = hash(llave);
		NodoHash<K,V> nuevo = new NodoHash(llave, valor);
		if(hashTable[pos] != null)
		{
			NodoHash<K, V> temp = hashTable[pos];
			hashTable[pos] = nuevo;
			hashTable[pos].setNextNode(temp);
			count ++;
		}
		else
		{
			hashTable[pos] = nuevo;
			count ++;
		}

		if(count >= factorCargaMax){
			resize();
		}
	}

	public V get(K llave)
	{
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V resp = null;
		int pos = hash(llave);
		if(hashTable[pos] != null)
		{
			boolean encontrado = false;
			Iterator<NodoHash<K, V>> ite = hashTable[pos].iterator();
			while(ite.hasNext() && encontrado == false)
			{
				NodoHash<K, V> actual = ite.next();
				if(actual.getLlave().compareTo(llave) == 0)
				{
					resp = actual.getValor();
					encontrado = true;
				}
			}
		}
		return resp;
	}


	public V delete(K llave)
	{
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones

		V resp = null;
		int pos = hash(llave);

		if(hashTable[pos] != null)
		{
			Iterator<NodoHash<K, V>> ite = hashTable[pos].iterator();
			boolean eliminado = false;
			NodoHash<K, V> anterior = null;
			while(ite.hasNext() && eliminado == false)
			{
				NodoHash<K, V> actual = ite.next();
				if(actual.getLlave().compareTo(llave) == 0)
				{
					if(anterior != null)
					{
						anterior.setNextNode(anterior.getNextNode().getNextNode());
					}
					else
					{
						hashTable[pos] = actual.getNextNode();
					}
					count --;
					eliminado = true;
				}
				anterior = actual;
			}
		}
		return resp;
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		int resp = 0;
		String key = llave.toString();
		String nuevo = "";
		for (int i = 0; i < key.length(); i++) 
		{
			nuevo += Character.getNumericValue(key.charAt(i));
		}
		resp = Integer.parseInt(nuevo);

		return resp % capacidad;
	}

//	private int hash(K llave)
//	{
//		String key = llave.toString();
//		int hash = 7;
//		for (int i = 0; i < key.length(); i++) 
//		{
//			hash = hash*31 + key.charAt(i);
//		}
//		return Math.abs(hash % capacidad);
//	}


	//TODO: Permita que la tabla sea dinamica
	private void resize()
	{
		capacidad *= 2;
		NodoHash<K, V>[] nueva = new NodoHash[capacidad];
		for (int i = 0; i < hashTable.length; i++) 
		{
			nueva[i] = new NodoHash<K, V>( hashTable[i].getLlave(), hashTable[i].getValor());
		}
		hashTable = nueva;
	}

}