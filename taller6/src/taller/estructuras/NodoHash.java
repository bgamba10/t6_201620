package taller.estructuras;

import java.util.Iterator;

public class NodoHash<K,V> {

	private K llave;
	private V valor;
	private NodoHash<K,V> nextNode; 
	
	public NodoHash(K llave, V valor) {
		super();
		this.llave = llave;
		this.valor = valor;
	}
	
	public K getLlave() {
		return llave;
	}
	
	public void setLlave(K llave) {
		this.llave = llave;
	}
	
	public V getValor() {
		return valor;
	}
	
	public void setValor(V valor) {
		this.valor = valor;
	}
	
	public NodoHash<K,V> getNextNode()
	{
		return nextNode;
	}
	
	public void setNextNode(NodoHash<K,V> pNextNode)
	{
		nextNode = pNextNode;
	}

	public Iterator<NodoHash<K,V>> iterator() 
	{
		return new HashIterator(this);
	}
	
	private class HashIterator implements Iterator<NodoHash<K,V>>
	{

		private NodoHash<K,V> actual;
		
		public HashIterator(NodoHash<K,V> head)
		{
			actual = head;
		}
		
		public boolean hasNext() 
		{
			boolean resp = false;
			if(actual != null)
				resp = true;
			return resp;
		}

		public NodoHash<K, V> next() 
		{
			NodoHash<K, V> resp = actual;
			actual = actual.getNextNode();
			return resp;
		}

		@Override
		public void remove() 
		{
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
