package taller.mundo;

public class Ciudadano 
{
	/**
	 * Nombre
	 */
	private String nombre;

	/**
	 * Apellido
	 */
	private String apellido;

	/**
	 * Teléfono
	 */
	private int telefono;
	
	public Ciudadano(String pNombre, String pApellido, int pTelefono)
	{
		nombre = pNombre;
		apellido = pApellido;
		telefono = pTelefono;
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public String darApellido()
	{
		return apellido;
	}
	
	public int darTelefono()
	{
		return telefono;
	}

	@Override
	public String toString() 
	{
		String resp = nombre + "\n" + apellido + "\n" + telefono; 
		return resp;
	}
}
