package taller.mundo;

import taller.estructuras.TablaHash;

public class Ciudad 
{
	/**
	 * 
	 */
	private TablaHash<String ,Ciudadano> habitantes;
	
	/**
	 * 
	 */
	public Ciudad()
	{
		habitantes = new TablaHash<String ,Ciudadano>();
	}
	
	/**
	 * 
	 * @return
	 */
	public TablaHash<String ,Ciudadano> darHabitantes()
	{
		return habitantes;
	}
	
	/**
	 * 
	 * @param pNombre
	 * @param pApellido
	 * @param pTelefono
	 */
	public void agregarHabitante(String pNombre, String pApellido, String pTelefono)
	{
		Ciudadano c = new Ciudadano(pNombre, pApellido, Integer.parseInt(pTelefono));
		habitantes.put(pTelefono, c);
	}
	
	/**
	 * 
	 * @param pNombre
	 * @param pApellido
	 * @param pTelefono
	 */
	public void agregarHabitanteNombre(String pNombre, String pApellido, String pTelefono)
	{
		Ciudadano c = new Ciudadano(pNombre, pApellido, Integer.parseInt(pTelefono));
		habitantes.put(pNombre, c);
	}
	
	/**
	 * 
	 * @param pLlave
	 * @return
	 */
	public Ciudadano[] buscarHabitante(String pLlave)
	{
		Ciudadano c[] = new Ciudadano[habitantes.cuenta()];
		c[0] = habitantes.get(pLlave);
		
		//para el caso de buscar por nombre
		int i = 0;
		
		while(habitantes.get(pLlave).darNombre().equals(pLlave))
		{
			c[i] = habitantes.get(pLlave);
		}
		return c;
	}

	public void agregarHabitanteLocalizacion(String pNombre, String pApellido, String pTelefono, String pLatitud,
			String pLongitud) {
		
		Ciudadano c = new Ciudadano(pNombre, pApellido, Integer.parseInt(pTelefono));
		double uno = Double.parseDouble(pLatitud);
		double dos = Double.parseDouble(pLongitud);
		int hash = (int) (uno*5 + dos*5);
		
		habitantes.put(hash+"", c);
		
	}
	
}
