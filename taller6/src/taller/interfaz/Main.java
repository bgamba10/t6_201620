package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import taller.mundo.Ciudad;
import taller.mundo.Ciudadano;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";

	private static Ciudad ciudad;

	public static void main(String[] args) 
	{
		long time = System.currentTimeMillis();

		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colisiones de "
				+ "la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio telefonico
			ciudad = new Ciudad();
			time = System.currentTimeMillis();
			int i = 0;
			entrada = br.readLine();
			while (entrada != null)
			{

				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información

				String[] datos = entrada.split(",");
				ciudad.agregarHabitante(datos[0], datos[1], datos[2]);
				ciudad.agregarHabitanteNombre(datos[0], datos[1], datos[2]);
				ciudad.agregarHabitanteLocalizacion(datos[0], datos[1], datos[2],datos[3], datos[4]);

				++i;
				if (++i%500000 == 0)
				{
					System.out.println(i + " entradas...");
					double m = System.currentTimeMillis() - time;
					System.out.println("Tiempo: " + m + " milis.");
				}
				entrada = br.readLine();
			}

			System.out.println(i + " entradas cargadas en total");
			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();
		}

		time = imprimirTiempo(time);
		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir){

			try 
			{

				String t1;
				String t2;
				String t3;

				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por nombre");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia

					System.out.println("Ingrese el nombre del ciudadano: ");
					t1 = br.readLine();

					System.out.println("Ingrese el apellido del ciudadano: ");
					t2 = br.readLine();

					System.out.println("Ingrese el número de teléfono: ");
					t3 = br.readLine();

					time = System.currentTimeMillis();
					ciudad.agregarHabitante(t1, t2, t3);
					time = imprimirTiempo(time);

					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente

					System.out.println("Ingrese el número de teléfono de su acudiente: ");
					t1 = br.readLine();

					time = System.currentTimeMillis();
					Ciudadano buscado = ciudad.buscarHabitante(t1)[0];

					if(buscado == null)
					{
						System.out.println("No existe un contacto con el número " + t1 
								+ " en el directorio.");
					}
					else
					{
						System.out.println(buscado.toString());
					}
					time = imprimirTiempo(time);

					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre

					System.out.println("Ingrese el nombre del ciudadano: ");
					t1 = br.readLine();

					time = System.currentTimeMillis();
					Ciudadano[] buscado2 = ciudad.buscarHabitante(t1);

					if(buscado2 == null)
					{
						System.out.println("No existe un contacto con el nombre " + t1 
								+ " en el directorio.");
					}
					else
					{
						int i = 0;
						while(buscado2[i] != null)
						{
							System.out.println(buscado2[i].toString());
							i++;
						}
					}
					time = imprimirTiempo(time);

					break;

				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual

					System.out.println("Ingrese localización del ciudadano (Latitud, Longitud): ");
					t1 = br.readLine();
					String[] datos = t1.split(",");
					double uno = Double.parseDouble(datos[0]);
					double dos = Double.parseDouble(datos[1]);
					int hash = (int) (uno*5 + dos*5);

					time = System.currentTimeMillis();
					Ciudadano[] buscado3 = ciudad.buscarHabitante(hash+"");

					if(buscado3 == null)
					{
						System.out.println("No existe un contacto con el nombre " + t1 
								+ " en el directorio.");
					}
					else
					{
						System.out.println(buscado3.toString());
					}
					time = imprimirTiempo(time);

					break;

				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		}
	}

	private static long imprimirTiempo(long time) 
	{
		double milis = System.currentTimeMillis()-time;
		System.out.println("Tiempo: " + milis + " milis.");
		return System.currentTimeMillis();
	}

}
